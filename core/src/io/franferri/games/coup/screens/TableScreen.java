package io.franferri.games.coup.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.badlogic.gdx.utils.DebugGame;
import com.badlogic.gdx.utils.DebugScreen;
import com.badlogic.gdx.utils.Utils;

public class TableScreen extends DebugScreen {

    DebugGame game;

    private OrthographicCamera orthographicCamera;
    private Viewport viewport;
    
    Texture background;

    Texture cardAmbassador;
    Texture cardAssassin;
    Texture cardCaptain;
    Texture cardContessa;
    Texture cardDuke;
    Texture cardInquisitor;
    Texture cardTreasuryReserve;


    private int screenWidth = 0;
    private int screenHeight = 0;

    public TableScreen() {

    }

    public TableScreen(DebugGame game) {
        this.game = (DebugGame) game;

        create();

    }

    public void create() {

        orthographicCamera = new OrthographicCamera();
        viewport = new FitViewport(1280, 720, orthographicCamera);

        background = new Texture("screens/table/background.png");
        //background.setWrap(Repeat, Repeat);

        cardAmbassador = Utils.resizeByWidth("screens/table/ambassador.png", 200);
        cardAssassin = Utils.resizeByWidth("screens/table/assassin.png", 200);
        cardCaptain = Utils.resizeByWidth("screens/table/captain.png", 200);
        cardContessa = Utils.resizeByWidth("screens/table/contessa.png", 200);
        cardDuke = Utils.resizeByWidth("screens/table/duke.png", 200);
        cardInquisitor = Utils.resizeByWidth("screens/table/inquisitor.png", 200);
        cardTreasuryReserve = Utils.resizeByHeight("screens/table/treasuryreserve.png", 200);
    }

    @Override
    public void show() {
        super.show();
    }

    @Override
    public void render(float delta) {
        super.render(delta);

        Utils.clearScreen();

        screenWidth = Gdx.graphics.getWidth();
        screenHeight = Gdx.graphics.getHeight();

        game.batch.setProjectionMatrix(orthographicCamera.combined);
        game.batch.begin();

        game.batch.draw(background, 0, 0, 0, 0, screenWidth, screenHeight);

        game.batch.draw(cardAmbassador, 100, 0);
        game.batch.draw(cardAssassin, 300, 0);
        game.batch.draw(cardCaptain, 500, 0);
        game.batch.draw(cardContessa, 700, 0);
        game.batch.draw(cardDuke, 100, 400);
        game.batch.draw(cardInquisitor, 300, 400);
        game.batch.draw(cardTreasuryReserve, 500, 400);

        game.batch.end();

    }


    @Override
    public void dispose() {
        super.dispose();

        game.batch.dispose();
        background.dispose();
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        viewport.update(width, height, true);
    }

    @Override
    public void pause() {
        super.pause();
    }

    @Override
    public void resume() {
        super.resume();
    }

    @Override
    public void hide() {
        super.hide();
    }


}
