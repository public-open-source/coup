package io.franferri.games.coup.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.badlogic.gdx.utils.DebugGame;
import com.badlogic.gdx.utils.DebugScreen;
import com.badlogic.gdx.utils.Utils;

import static com.badlogic.gdx.graphics.Texture.TextureWrap.Repeat;

public class MainMenuScreen extends DebugScreen {

    DebugGame game;

    private OrthographicCamera orthographicCamera;
    private Viewport viewport;

    Texture background;
    Texture couplogo;
    Texture contessa;
    Texture challengeblack;
    Texture joinachallengeblack;
    Texture challengeorange;
    Texture joinachallengeorange;

    private int screenWidth = 0;
    private int screenHeight = 0;

    public MainMenuScreen(DebugGame game) {

        this.game = (DebugGame) game;

        orthographicCamera = new OrthographicCamera();
        viewport = new FitViewport(1280, 720, orthographicCamera);

        background = new Texture("screens/mainmenu/background.png");
        background.setWrap(Repeat, Repeat);

        couplogo = new Texture("screens/mainmenu/couplogo.png");
        contessa = Utils.resizeByWidth("screens/mainmenu/contessa.png", 250);

        challengeblack = Utils.resizeByWidth("screens/mainmenu/challengeblack.png", 350);
        challengeorange = Utils.resizeByWidth("screens/mainmenu/challengeorange.png", 350);

        joinachallengeblack = Utils.resizeByWidth("screens/mainmenu/joinachallengeblack.png", 500);
        joinachallengeorange = Utils.resizeByWidth("screens/mainmenu/joinachallengeorange.png", 500);

    }

    @Override
    public void show() {
        super.show();
    }

    @Override
    public void render(float delta) {
        super.render(delta);

        Utils.clearScreen();

        screenWidth = Gdx.graphics.getWidth();
        screenHeight = Gdx.graphics.getHeight();

        game.batch.setProjectionMatrix(orthographicCamera.combined);
        game.batch.begin();

        game.batch.draw(background, 0, 0, 0, 0, screenWidth, screenHeight);

        game.batch.draw(couplogo, Utils.centerInScreenX(screenWidth, couplogo), screenHeight - 150);
        game.batch.draw(contessa, screenWidth - contessa.getWidth(), 0);

        int challengeXPosition = Utils.centerInScreenX(screenWidth, challengeblack);
        int challengeYPosition = 400;
        if (
                Gdx.input.getX() >= challengeXPosition
                        && Gdx.input.getX() < challengeXPosition + challengeorange.getWidth()
                        && Gdx.input.getY() >= screenHeight - challengeYPosition - challengeorange.getHeight()
                        && Gdx.input.getY() < screenHeight - challengeYPosition) {
            game.batch.draw(challengeorange, challengeXPosition, challengeYPosition);

            if (Gdx.input.isTouched()) {
                game.setScreen(new TableScreen(game));
            }

        } else {
            game.batch.draw(challengeblack, challengeXPosition, challengeYPosition);
        }

        int joinachallengeXPosition = Utils.centerInScreenX(screenWidth, joinachallengeblack);
        int joinachallengeYPosition = 200;
        if (
                Gdx.input.getX() >= joinachallengeXPosition
                        && Gdx.input.getX() < joinachallengeXPosition + joinachallengeorange.getWidth()
                        && Gdx.input.getY() >= screenHeight - joinachallengeYPosition - joinachallengeorange.getHeight()
                        && Gdx.input.getY() < screenHeight - joinachallengeYPosition) {
            game.batch.draw(joinachallengeorange, joinachallengeXPosition, joinachallengeYPosition);
        } else {
            game.batch.draw(joinachallengeblack, joinachallengeXPosition, joinachallengeYPosition);
        }


        game.batch.end();

    }

    @Override
    public void dispose() {
        super.dispose();

        game.batch.dispose();

        background.dispose();
        couplogo.dispose();
        contessa.dispose();
        challengeblack.dispose();
        joinachallengeblack.dispose();
        challengeorange.dispose();
        joinachallengeorange.dispose();
    }


    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        viewport.update(width, height, true);
    }

    @Override
    public void pause() {
        super.pause();
    }

    @Override
    public void resume() {
        super.resume();
    }

    @Override
    public void hide() {
        super.dispose();
    }

}
