package io.franferri.games.coup.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.badlogic.gdx.utils.DebugGame;
import com.badlogic.gdx.utils.DebugScreen;
import com.badlogic.gdx.utils.Utils;

public class FontsExamplesScreen extends DebugScreen {

    DebugGame game;

    private OrthographicCamera orthographicCamera;
    private Viewport viewport;
    private BitmapFont bitmapFontAGaramondProBold;
    private BitmapFont bitmapFontAGaramondProBoldItalic;
    private BitmapFont bitmapFontAGaramondProItalic;
    private BitmapFont bitmapFontAGaramondProRegular;
    private BitmapFont bitmapFontEthnocentricRG;
    private BitmapFont bitmapFontPrototype;
    private BitmapFont bitmapFontTrajanProBold;


    public FontsExamplesScreen(Game game) {

        this.game = (DebugGame) game;

        orthographicCamera = new OrthographicCamera();
        viewport = new FitViewport(1280, 720, orthographicCamera);
        bitmapFontAGaramondProBold = Utils.fontToBitmapFont("fonts/agaramondprobold.otf", 24);
        bitmapFontAGaramondProBoldItalic = Utils.fontToBitmapFont("fonts/agaramondprobolditalic.otf", 24);
        bitmapFontAGaramondProItalic = Utils.fontToBitmapFont("fonts/agaramondproitalic.otf", 24);
        bitmapFontAGaramondProRegular = Utils.fontToBitmapFont("fonts/agaramondproregular.otf", 24);
        bitmapFontEthnocentricRG = Utils.fontToBitmapFont("fonts/ethnocentricrg.ttf", 24);
        bitmapFontPrototype = Utils.fontToBitmapFont("fonts/prototype.ttf", 24);
        bitmapFontTrajanProBold = Utils.fontToBitmapFont("fonts/trajanprobold.ttf", 24);

    }

    @Override
    public void show() {
        super.show();
    }

    @Override
    public void render(float delta) {
        super.render(delta);

        Utils.clearScreen();

        game.batch.setProjectionMatrix(orthographicCamera.combined);
        game.batch.begin();

        draw();

        game.batch.end();

    }

    private void draw() {

        // Mouse / Touch
        int mouseX = Gdx.input.getX();
        int mouseY = Gdx.input.getY();

        // Buttons
        boolean isLeftButtonPressed = Gdx.input.isButtonPressed(Input.Buttons.LEFT);
        boolean isRightButtonPressed = Gdx.input.isButtonPressed(Input.Buttons.RIGHT);

        // Keys
        boolean wKeyPressed = Gdx.input.isKeyPressed(Input.Keys.W);
        boolean sKeyPressed = Gdx.input.isKeyPressed(Input.Keys.S);

        //AGaramondProBold

        bitmapFontAGaramondProBold.draw(game.batch,
                "Mouse/Touch x=" + mouseX + " y=" + mouseY,
                20f,
                720 - 20f);

        bitmapFontAGaramondProBold.draw(game.batch,
                isLeftButtonPressed ? "Left button pressed" : "Left button not pressed",
                20f,
                720 - 50f);

        bitmapFontAGaramondProBold.draw(game.batch,
                isRightButtonPressed ? "Right button pressed" : "Right button not pressed",
                20f,
                720 - 80f);

        bitmapFontAGaramondProBold.draw(game.batch,
                wKeyPressed ? "W key pressed" : "W key not pressed",
                20f,
                720 - 110f);

        bitmapFontAGaramondProBold.draw(game.batch,
                sKeyPressed ? "S key pressed" : "S key not pressed",
                20f,
                720 - 140f);

//


        bitmapFontAGaramondProBoldItalic.draw(game.batch,
                "Mouse/Touch x=" + mouseX + " y=" + mouseY,
                420f,
                720 - 20f);

        bitmapFontAGaramondProBoldItalic.draw(game.batch,
                isLeftButtonPressed ? "Left button pressed" : "Left button not pressed",
                420f,
                720 - 50f);

        bitmapFontAGaramondProBoldItalic.draw(game.batch,
                isRightButtonPressed ? "Right button pressed" : "Right button not pressed",
                420f,
                720 - 80f);

        bitmapFontAGaramondProBoldItalic.draw(game.batch,
                wKeyPressed ? "W key pressed" : "W key not pressed",
                420f,
                720 - 110f);

        bitmapFontAGaramondProBoldItalic.draw(game.batch,
                sKeyPressed ? "S key pressed" : "S key not pressed",
                420f,
                720 - 140f);


        //

        bitmapFontAGaramondProItalic.draw(game.batch,
                "Mouse/Touch x=" + mouseX + " y=" + mouseY,
                820f,
                720 - 20f);

        bitmapFontAGaramondProItalic.draw(game.batch,
                isLeftButtonPressed ? "Left button pressed" : "Left button not pressed",
                820f,
                720 - 50f);

        bitmapFontAGaramondProItalic.draw(game.batch,
                isRightButtonPressed ? "Right button pressed" : "Right button not pressed",
                820f,
                720 - 80f);

        bitmapFontAGaramondProItalic.draw(game.batch,
                wKeyPressed ? "W key pressed" : "W key not pressed",
                820f,
                720 - 110f);

        bitmapFontAGaramondProItalic.draw(game.batch,
                sKeyPressed ? "S key pressed" : "S key not pressed",
                820f,
                720 - 140f);


        //

        bitmapFontAGaramondProRegular.draw(game.batch,
                "Mouse/Touch x=" + mouseX + " y=" + mouseY,
                20f,
                720 - 220f);

        bitmapFontAGaramondProRegular.draw(game.batch,
                isLeftButtonPressed ? "Left button pressed" : "Left button not pressed",
                20f,
                720 - 250f);

        bitmapFontAGaramondProRegular.draw(game.batch,
                isRightButtonPressed ? "Right button pressed" : "Right button not pressed",
                20f,
                720 - 280f);

        bitmapFontAGaramondProRegular.draw(game.batch,
                wKeyPressed ? "W key pressed" : "W key not pressed",
                20f,
                720 - 310f);

        bitmapFontAGaramondProRegular.draw(game.batch,
                sKeyPressed ? "S key pressed" : "S key not pressed",
                20f,
                720 - 340f);


        //


        bitmapFontEthnocentricRG.draw(game.batch,
                "Mouse/Touch x=" + mouseX + " y=" + mouseY,
                20f,
                720 - 420f);

        bitmapFontEthnocentricRG.draw(game.batch,
                isLeftButtonPressed ? "Left button pressed" : "Left button not pressed",
                20f,
                720 - 450f);

        bitmapFontEthnocentricRG.draw(game.batch,
                isRightButtonPressed ? "Right button pressed" : "Right button not pressed",
                20f,
                720 - 480f);

        bitmapFontEthnocentricRG.draw(game.batch,
                wKeyPressed ? "W key pressed" : "W key not pressed",
                20f,
                720 - 510f);

        bitmapFontEthnocentricRG.draw(game.batch,
                sKeyPressed ? "S key pressed" : "S key not pressed",
                20f,
                720 - 540f);


        //


        bitmapFontPrototype.draw(game.batch,
                "Mouse/Touch x=" + mouseX + " y=" + mouseY,
                420f,
                720 - 220f);

        bitmapFontPrototype.draw(game.batch,
                isLeftButtonPressed ? "Left button pressed" : "Left button not pressed",
                420f,
                720 - 250f);

        bitmapFontPrototype.draw(game.batch,
                isRightButtonPressed ? "Right button pressed" : "Right button not pressed",
                420f,
                720 - 280f);

        bitmapFontPrototype.draw(game.batch,
                wKeyPressed ? "W key pressed" : "W key not pressed",
                420f,
                720 - 310f);

        bitmapFontPrototype.draw(game.batch,
                sKeyPressed ? "S key pressed" : "S key not pressed",
                420f,
                720 - 340f);

        //


        bitmapFontTrajanProBold.draw(game.batch,
                "Mouse/Touch x=" + mouseX + " y=" + mouseY,
                820f,
                720 - 220f);

        bitmapFontTrajanProBold.draw(game.batch,
                isLeftButtonPressed ? "Left button pressed" : "Left button not pressed",
                820f,
                720 - 250f);

        bitmapFontTrajanProBold.draw(game.batch,
                isRightButtonPressed ? "Right button pressed" : "Right button not pressed",
                820f,
                720 - 280f);

        bitmapFontTrajanProBold.draw(game.batch,
                wKeyPressed ? "W key pressed" : "W key not pressed",
                820f,
                720 - 310f);

        bitmapFontTrajanProBold.draw(game.batch,
                sKeyPressed ? "S key pressed" : "S key not pressed",
                820f,
                720 - 340f);

    }

    @Override
    public void dispose() {
        super.dispose();

        game.batch.dispose();

        bitmapFontAGaramondProBold.dispose();
        bitmapFontAGaramondProBoldItalic.dispose();
        bitmapFontAGaramondProItalic.dispose();
        bitmapFontAGaramondProRegular.dispose();
        bitmapFontEthnocentricRG.dispose();
        bitmapFontPrototype.dispose();
        bitmapFontTrajanProBold.dispose();
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        viewport.update(width, height, true);
    }

    @Override
    public void pause() {
        super.pause();
    }

    @Override
    public void resume() {
        super.resume();
    }

    @Override
    public void hide() {
        super.dispose();
    }

}
