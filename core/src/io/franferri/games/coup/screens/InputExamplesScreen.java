package io.franferri.games.coup.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.badlogic.gdx.utils.DebugGame;
import com.badlogic.gdx.utils.DebugScreen;
import com.badlogic.gdx.utils.Utils;

public class InputExamplesScreen extends DebugScreen implements InputProcessor {

    DebugGame game;

    private OrthographicCamera orthographicCamera;
    private Viewport viewport;
    private BitmapFont bitmapFontEthnocentricRG;

    private static final int MAX_MESSAGE_COUNT = 15;
    private final Array<String> messages = new Array<>();

    public InputExamplesScreen(DebugGame game) {

        this.game = (DebugGame) game;

        orthographicCamera = new OrthographicCamera();
        viewport = new FitViewport(1280, 720, orthographicCamera);
        bitmapFontEthnocentricRG = Utils.fontToBitmapFont("fonts/ethnocentricrg.ttf", 24);

        // ONE INPUT PROCESSOR EXAMPLES
        // -----------------------------------------
        // Example A
        //Gdx.input.setInputProcessor(this);

        // Example B
        //Gdx.input.setInputProcessor(new InputAdapter() {
        //    @Override
        //    public boolean keyDown(int keycode) {
        //        log("keyDown keycode= " + keycode);
        //        return true;
        //    }
        //});

        // MULTIPLE INPUT PROCESSORS EXAMPLES
        // If our methods return false, will stop processing the input
        // -----------------------------------------
        InputMultiplexer multiplexer = new InputMultiplexer();

        InputAdapter firstProcessor = new InputAdapter() {
            public boolean keyDown(int keycode) {
                log("first - keyDown keycode= " + keycode);
                return true;
            }


            public boolean keyUp(int keycode) {
                log("first - keyUp keycode= " + keycode);
                return false;
            }

            public boolean keyTyped(char character) {
                log("first - keyTyped character= " + character);
                return false;
            }


        };
        InputAdapter secondProcessor = new InputAdapter() {
            public boolean keyDown(int keycode) {
                log("second - keyDown keycode= " + keycode);
                return true;
            }


            public boolean keyUp(int keycode) {
                log("second - keyUp keycode= " + keycode);
                return false;
            }

            public boolean keyTyped(char character) {
                log("second - keyTyped character= " + character);
                return false;
            }

        };

        multiplexer.addProcessor(firstProcessor);
        multiplexer.addProcessor(secondProcessor);

        Gdx.input.setInputProcessor(multiplexer);

    }

    private void draw() {

        for (int i = 0; i < messages.size; i++) {
            bitmapFontEthnocentricRG.draw(game.batch,
                    messages.get(i),
                    20f,
                    720 - 40f * (i + 1));
        }

    }

    private void addMessage(String message) {
        messages.add(message);

        if (messages.size > MAX_MESSAGE_COUNT) {
            messages.removeIndex(0);
        }
    }

    @Override
    public void show() {
        super.show();
    }

    @Override
    public void render(float delta) {
        super.render(delta);

        Utils.clearScreen();

        game.batch.setProjectionMatrix(orthographicCamera.combined);
        game.batch.begin();

        draw();

        game.batch.end();

    }

    @Override
    public void dispose() {
        super.dispose();
        game.batch.dispose();
    }


    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        viewport.update(width, height, true);
    }

    @Override
    public void pause() {
        super.pause();
    }

    @Override
    public void resume() {
        super.resume();
    }

    @Override
    public void hide() {
        super.dispose();
    }


    @Override
    public boolean keyDown(int keycode) {
        String message = "keyDown keycode=" + keycode;
        log(message);
        addMessage(message);
        return true;
    }

    @Override
    public boolean keyUp(int keycode) {
        String message = "keyUp keycode=" + keycode;
        log(message);
        addMessage(message);
        return true;
    }

    @Override
    public boolean keyTyped(char character) {
        String message = "keyTyped character=" + character;
        log(message);
        addMessage(message);
        return true;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        String message = "touchDown screenX=" + screenX + "screenY=" + screenY;
        log(message);
        addMessage(message);
        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        String message = "touchUp screenX=" + screenX + "screenY=" + screenY;
        log(message);
        addMessage(message);
        return true;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        String message = "touchDragged screenX=" + screenX + "screenY=" + screenY;
        log(message);
        addMessage(message);
        return true;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        String message = "mouseMoved screenX=" + screenX + "screenY=" + screenY;
        log(message);
        addMessage(message);
        return true;
    }

    @Override
    public boolean scrolled(int amount) {
        String message = "keyDown amount=" + amount;
        log(message);
        addMessage(message);
        return true;
    }
}
