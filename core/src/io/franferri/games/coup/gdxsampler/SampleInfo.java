package io.franferri.games.coup.gdxsampler;

import com.badlogic.gdx.Game;

public class SampleInfo {

    private final String name;
    private final Class<? extends Game> clazz;

    public SampleInfo(Class<? extends Game> clazz) {
        this.clazz = clazz;
        name = clazz.getSimpleName();
    }

    public String getName() {
        return name;
    }

    public Class<?> getClazz() {
        return clazz;
    }
}
