package io.franferri.games.coup.gdxsampler;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.utils.reflect.ClassReflection;

public class SampleFactory {

    public static Game newSample(String name) {

        if (name == null || name.isEmpty()) {
            throw new IllegalArgumentException("name param is required.");
        }

        SampleInfo info = SampleInfos.find(name);

        try {
            return (Game) ClassReflection.newInstance(info.getClazz());
        } catch (Exception e) {
            throw new RuntimeException("Cannot create sample with name=" + name, e);
        }

    }

    private SampleFactory() {
    }

}
