package io.franferri.games.coup.gdxsampler;

import io.franferri.games.coup.COUPMain;
import io.franferri.games.coup.OrthographicCameraTestMain;

import java.util.*;

public class SampleInfos {

    public static final List<SampleInfo> ALL = Arrays.asList(
            COUPMain.SAMPLE_INFO,
            OrthographicCameraTestMain.SAMPLE_INFO
    );

    public static List<String> getSampleNames() {
        List<String> ret = new ArrayList<>();

        for (SampleInfo info : ALL) {
            ret.add(info.getName());
        }

        Collections.sort(ret);
        return ret;
    }

    ;

    public static SampleInfo find(String name) {

        if (null == name || name.isEmpty()) {
            throw new IllegalArgumentException("Name argument is required.");
        }

        SampleInfo ret = null;
        for (SampleInfo info : ALL) {
            if (info.getName().equals(name)) {
                ret = info;
                break;
            }
        }

        if (ret == null) {
            throw new IllegalArgumentException("Could not find sample with name= " + name);
        }

        return ret;

    }

    private SampleInfos() {
    }


}
