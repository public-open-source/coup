package io.franferri.games.coup;

import io.franferri.games.coup.gdxsampler.SampleInfo;
import io.franferri.games.coup.screens.MainMenuScreen;
import com.badlogic.gdx.utils.DebugGame;

public class COUPMain extends DebugGame {

    public static final SampleInfo SAMPLE_INFO = new SampleInfo(COUPMain.class);

    @Override
    public void create() {
        super.create();

        setScreen(new MainMenuScreen(this));
    }

}
