package io.franferri.games.coup;

import io.franferri.games.coup.gdxsampler.SampleInfo;
import io.franferri.games.coup.screens.FontsExamplesScreen;
import com.badlogic.gdx.utils.DebugGame;

public class OrthographicCameraTestMain extends DebugGame {

    public static final SampleInfo SAMPLE_INFO = new SampleInfo(OrthographicCameraTestMain.class);

    @Override
    public void create() {
        super.create();

        //setScreen(new MainMenuScreen(this));
        setScreen(new FontsExamplesScreen(this));
    }

}
