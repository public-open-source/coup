package com.badlogic.gdx.utils;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;

public abstract class DebugScreen implements Screen {

    String className = this.getClass().getSimpleName();

    public DebugScreen() {
        Gdx.app.debug(className + " constructor", "");
    }

    @Override
    public void show() {
        Gdx.app.debug(className + " .show()", "");
    }

    boolean renderLoopLogs = true;


    @Override
    public void render(float delta) {
        if (renderLoopLogs) {
            Gdx.app.debug(className + " .render()", "");
            renderLoopLogs = false;
        }
    }

    @Override
    public void resize(int width, int height) {
        Gdx.app.debug(className + " .resize()", "");
    }

    @Override
    public void pause() {
        Gdx.app.debug(className + " .pause()", "");
    }

    @Override
    public void resume() {
        Gdx.app.debug(className + " .resume()", "");
    }

    @Override
    public void hide() {
        Gdx.app.debug(className + " .hide()", "");
    }

    @Override
    public void dispose() {
        Gdx.app.debug(className + ".dispose()", "");
    }

    protected void log(String message) {
        Gdx.app.debug(className + ".log()", message);
    }

}
