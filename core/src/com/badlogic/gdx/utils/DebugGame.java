package com.badlogic.gdx.utils;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.viewport.Viewport;
import io.franferri.games.coup.gdxsampler.SampleInfo;
import io.franferri.games.coup.screens.FontsExamplesScreen;

public class DebugGame extends Game {

    public SpriteBatch batch;

    String className = this.getClass().getSimpleName();

    @Override
    public void create() {

        if (Gdx.app.getType() == Application.ApplicationType.Desktop) {
            Gdx.app.setLogLevel(Application.LOG_DEBUG);
        }

        Gdx.app.debug(className + " .create()", "");

        batch = new SpriteBatch();
    }

}
