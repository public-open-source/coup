package com.badlogic.gdx.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;

public class Utils {

    private Utils() {
    }

    public static void clearScreen() {
        clearScreen(Color.BLACK);
    }

    public static void clearScreen(Color color) {
        Gdx.gl.glClearColor(color.r, color.g, color.b, color.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    }

    public static BitmapFont fontToBitmapFont(String internalPath, int size) {
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal(internalPath));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = size;
        BitmapFont bitmapFont = generator.generateFont(parameter);
        generator.dispose();
        return bitmapFont;
    }

    private static int calculateHeightByAspectRatio(Pixmap pixmap, int width) {
        return (int) (pixmap.getHeight() / (pixmap.getWidth() / (float) width));
    }

    private static int calculateWidthByAspectRatio(Pixmap pixmap, int height) {
        return (int) (pixmap.getWidth() / (pixmap.getHeight() / (float) height));
    }

    public static Texture resizeByWidth(String imagePath, int resizeWidth) {

        Pixmap pixmapOriginal = new Pixmap(Gdx.files.internal(imagePath));

        int height = calculateHeightByAspectRatio(pixmapOriginal, resizeWidth);

        Pixmap pixmapResized = new Pixmap(resizeWidth, height, pixmapOriginal.getFormat());
        pixmapResized.drawPixmap(pixmapOriginal,
                0, 0, pixmapOriginal.getWidth(), pixmapOriginal.getHeight(),
                0, 0, pixmapResized.getWidth(), pixmapResized.getHeight()
        );

        Texture texture = new Texture(pixmapResized);

        pixmapOriginal.dispose();
        pixmapResized.dispose();

        return texture;
    }

    public static Texture resizeByHeight(String imagePath, int resizeHeight) {

        Pixmap pixmapOriginal = new Pixmap(Gdx.files.internal(imagePath));

        int width = calculateWidthByAspectRatio(pixmapOriginal, resizeHeight);

        Pixmap pixmapResized = new Pixmap(width, resizeHeight, pixmapOriginal.getFormat());
        pixmapResized.drawPixmap(pixmapOriginal,
                0, 0, pixmapOriginal.getWidth(), pixmapOriginal.getHeight(),
                0, 0, pixmapResized.getWidth(), pixmapResized.getHeight()
        );

        Texture texture = new Texture(pixmapResized);

        pixmapOriginal.dispose();
        pixmapResized.dispose();

        return texture;
    }

    public static int centerInScreenX(int screenWidth, Texture texture) {
        return screenWidth / 2 - texture.getWidth() / 2;
    }


}
